// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "NocturnalGameGameMode.generated.h"

UCLASS(minimalapi)
class ANocturnalGameGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ANocturnalGameGameMode();
};



