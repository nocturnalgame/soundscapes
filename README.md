# README #

Welcome to the Team Repository!

## What is this repository for? ##

This Repository is being used to track changes in the development of the project. 

### Version: null ###

## How do I get set up? ##

Here is the following steps you will need to perform to get the project working!

### Dependencies ###

1. UE4 4.10.1 / 4.10.2
2. Visual Studio 2015 with C++.
3. SourceTree for Source Control.

### SourceTree Configuration ###

1. Open SourceTree and log in with your bitbucket account.
2. Log in to bitbucket online.
3. In your Overview you will have a link to nocturnalgame / Soundscapes. Click it.
4. On the left under Navigation. Click on Branches.
5. Once there, change your Filters to "Merged" and Click on the link to "WorkingBranch"
6. Click the blue button that says "Check out in SourceTree" Click yes when it asks you to open the application.
7. The software will open with a pop up box called URL Actions. Make sure "Clone New" is selected. Choose your destination path - this is where the local copy of the project will be on. After you make changes to it, we won't see them until they are "commited" on the branch.
8. Click the button "Clone". It will now do its thing...
9. Once finished you can close the output window and begin working on the project.

@Doman1112

### Working Branch ###

All commits should be done on your local branch. Do not -push- until you have been given the all clear.

## Contribution guidelines ##

### Tests ###

Programmers should be testing as they go. Make sure for each new functionality you wish to add, it does not cause more problems with existing code!

### Code Review ###

As Programming starts, it will be important to have regular code reviews to ensure that everyone is adhering to the team styles and no mistakes are being made.

This will likely take place once a week.

### Adding assets / code ###

Everyone should be working on a section of the project. Please speak to your relevant Team Lead to see where you should be working and where it should be commited to. Programmers speak with @Doman1112, Artists with @The-Honorable-Joris-Bohnson.


## Who do I talk to? ##

Any issues with pushing to your local branches or trouble with SourceTree, contact a programmer to help solve it!


Editor: @CJoriginal